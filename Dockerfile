FROM php:8.1-alpine3.15

MAINTAINER "Kais Selmi" <selmi.kaies@gmail.com>

RUN apk --no-cache update && apk --no-cache add bash git && \
    wget https://get.symfony.com/cli/installer -O - | bash && \
    mv /root/.symfony/bin/symfony /usr/local/bin/symfony && \
    git init --bare

ENTRYPOINT ["symfony"]
